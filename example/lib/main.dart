import 'package:flutter/material.dart';
import 'package:flutter_dipnav/flutter_dipnav.dart';

void main() {
  runApp(MyApp());
}

final notFoundRoute = DipNavRoute(
    path: '/404',
    page: MyHomePage(),
    arguments: {'title': 'Error 404 - Not found page'});

final initRoute = DipNavRoute(
    path: '/', page: MyHomePage(), arguments: {'title': 'Home Page 1'});

final pageRoute2 = DipNavRoute(
    path: '/page2', page: MyHomePage(), arguments: {'title': 'Home Page 2'});

final pageRoute3 = DipNavRoute(
    path: '/page3',
    page: MyHomePage(),
    queries: {'type': 'replaced'},
    arguments: {'title': 'Home Page 3'});

final pageRoute4 = DipNavRoute(
    path: '/page4',
    page: MyHomePage(),
    queries: {'type': 'cleaned_stack'},
    arguments: {'title': 'Home Page 4'});

final pageRoute5 = DipNavRoute(
  path: '/page5',
  page: MyHomePage(),
  arguments: {'title': 'Home Page 5'},
);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DipNav.builder(
      initRoute: initRoute,
      builder: (delegate, parser, dispatcher) => MaterialApp.router(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routerDelegate: delegate,
        routeInformationParser: parser,
        backButtonDispatcher: dispatcher,
      ),
      notFoundRoute: notFoundRoute,
      routes: [pageRoute2, pageRoute3, pageRoute4, pageRoute5],
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String title = '';

    final args = context.dipNavData?.arguments;
    if (args != null && args is Map) {
      title = args['title'].toString();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (context.dipNavData?.queries != null)
            Text('queries :${context.dipNavData?.queries}'),
          TextButton(
              onPressed: () => context.dipNav.pop(),
              child: Text('Previous page')),
          TextButton(
              onPressed: () => context.dipNav.pushRoute(
                    route: pageRoute3,
                    replaceCurrent: true,
                  ),
              child: Text('Replace page')),
          TextButton(
              onPressed: () => context.dipNav.pushRoute(
                    route: pageRoute4,
                    removeUntil: true,
                  ),
              child: Text('Remove until new page')),
          TextButton(
              onPressed: () => context.dipNav.pushRoute(
                    route: pageRoute5,
                    replaceCurrent: true,
                    rebuild: false,
                  ),
              child: Text('Change state witout rebuild')),
          TextButton(
              onPressed: () => context.dipNav.pushRoute(route: pageRoute2),
              child: Text('Next page')),
        ],
      )),
    );
  }
}
