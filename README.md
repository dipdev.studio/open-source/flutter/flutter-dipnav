# flutter_dipnav

Advanced navigation based on Navigator 2.0

+ **Simple use**
You only need two methods - push and pop
+ **Easy integration**
Just two steps of implementation
+ **Lots of possibilities**
Protect routes, for example, to be accessed only by authorized

![Example of work](https://gitlab.com/dipdev.studio/open-source/flutter/flutter-dipnav/-/raw/master/images/example-of-work.gif)

## Using

### Go to the next route

```dart
context.dipNav.push(
    path: '/',
    page: MyHomePage(title: 'Home Page'),
);
```

### Go to the previous route

```dart
context.dipNav.pop();
```

### Get queries from the route

```dart
context.dipNavData?.queries
```

### Get arguments from the route

```dart
context.dipNavData?.arguments
```

## Getting Started

### 1) Add dependency

```yaml
dependencies:
  ...
  flutter_dipnav: ^1.0.0-dev.10
```

### 2) Add the [DipNav] widget to the source, like this

```dart
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DipNav.builder(
      initRoute: DipNavRoute(
          path: '/',
          page: MyHomePage(
            title: 'Home Page 1',
          )),
      builder: (delegate, parser, dispatcher) => MaterialApp.router(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routerDelegate: delegate,
        routeInformationParser: parser,
        backButtonDispatcher: dispatcher,
      ),
      notFoundRoute: DipNavRoute(
          path: '/404',
          page: MyHomePage(
            title: 'Error 404 - Not found page',
          )),
    );
  }
}
```

###### Please check the [example][example]

[example]: https://gitlab.com/dipdev.studio/open-source/flutter/flutter-dipnav/-/tree/master/example

## Features and bugs

> Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://gitlab.com/dipdev.studio/open-source/flutter/flutter-dipnav/-/issues

## Authors

> This project developed by [**DipDev Studio**](https://dipdev.studio) Team: [@Dimoshka](https://www.linkedin.com/in/dmytroprylutskyi/)
