import 'package:flutter/widgets.dart';
import 'package:flutter_dipnav/src/dip_nav_data.dart';

/// A class of configuration [PageRoute]
///
/// [guard] this callback should return true if navigation is permission
class DipNavRoute extends PageRoute {
  final String? key;
  final String path;
  final Widget page;
  final DipNavData? data;
  final bool Function()? guard;

  DipNavRoute({
    this.key,
    required this.path,
    required this.page,
    Map<String, String>? queries,
    Object? arguments,
    this.guard,
  })  : data = DipNavData(queries: queries, arguments: arguments),
        super(
            settings:
                RouteSettings(name: path, arguments: arguments ?? queries));

  Uri get uri => Uri(path: path, queryParameters: data?.queries);

  @override
  Color? get barrierColor => null;

  @override
  String? get barrierLabel => null;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return page;
  }

  @override
  bool get maintainState => true;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 300);
}
