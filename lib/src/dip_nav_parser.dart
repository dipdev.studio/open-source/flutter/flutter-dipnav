part of 'dip_nav_router.dart';

/// A delegate that is used by the [Router] widget to parse a route information
/// into a configuration of type [DipNavRoute].
class DipNavParser extends RouteInformationParser<DipNavRoute> {
  /// {@macro flutter_dipnav.DipNavRouter}
  final DipNavRouter _delegate;

  const DipNavParser(this._delegate);

  /// Converts the given route information into parsed data to pass to a
  /// [RouterDelegate].
  @override
  Future<DipNavRoute> parseRouteInformation(
      RouteInformation routeInformation) async {
    final location = routeInformation.location;
    if (location == null) {
      return _delegate.initRoute;
    }
    final uri = Uri.parse(location);
    if (uri.pathSegments.isEmpty) {
      return _delegate.initRoute;
    }
    DipNavRoute? foundRoute;
    for (var route in _delegate.routes) {
      if (route.path == uri.path) {
        foundRoute = route;
        break;
      }
    }
    final isGradted = foundRoute?.guard?.call() ?? true;
    return isGradted
        ? foundRoute ?? _delegate.notFoundRoute ?? _delegate.initRoute
        : _delegate.notFoundRoute ?? _delegate.initRoute;
  }

  /// Restore the route information from the given configuration.
  @override
  RouteInformation? restoreRouteInformation(DipNavRoute configuration) {
    return RouteInformation(location: configuration.uri.toString());
  }
}
