part of 'dip_nav_router.dart';

/// Report to a [Router] when the user taps the back button on platforms that
/// support back buttons (such as Android).
class DipNavDispatcher extends RootBackButtonDispatcher {
  /// {@macro flutter_dipnav.DipNavRouter}
  final DipNavRouter _delegate;

  DipNavDispatcher(this._delegate) : super();

  @override
  Future<bool> didPopRoute() {
    return _delegate.pop();
  }
}
