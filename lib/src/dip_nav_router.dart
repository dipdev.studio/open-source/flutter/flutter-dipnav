import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dipnav/flutter_dipnav.dart';
import 'package:flutter_dipnav/src/dip_nav_route.dart';

part 'dip_nav_delegate.dart';
part 'dip_nav_dispatcher.dart';
part 'dip_nav_parser.dart';
part 'dip_nav_resultable_page.dart';

/// Main class of routing based on Navigator 2.0.
class DipNavRouter {
  late DipNavDelegate _delegate;
  late DipNavParser _parser;
  late DipNavDispatcher _dispatcher;

  DipNavRouter(
      {GlobalKey<NavigatorState>? key,
      required DipNavRoute initRoute,
      DipNavRoute? notFoundRoute,
      List<DipNavRoute>? routes,
      List<NavigatorObserver>? dipNavObservers,
      TransitionDelegate<dynamic>? transitionDelegate}) {
    final routesList = List.of(routes ?? <DipNavRoute>[]);

    _delegate = DipNavDelegate(
      key: key,
      initRoute: initRoute,
      notFoundRoute: notFoundRoute,
      routes: routesList,
      dipNavObservers: dipNavObservers,
      transitionDelegate: transitionDelegate,
    );
    _parser = DipNavParser(this);
    _dispatcher = DipNavDispatcher(this);
  }

  /// {@macro flutter_dipnav._DipNavDelegate}
  DipNavDelegate get delegate => _delegate;

  /// {@macro flutter_dipnav._DipNavParser}
  DipNavParser get parser => _parser;

  /// {@macro flutter_dipnav._DipNavDispatcher}
  DipNavDispatcher get dispatcher => _dispatcher;

  /// {@macro flutter_dipnav._DipNavRouter.routes}
  List<DipNavRoute> get routes => List.of(_delegate.routes);

  /// {@macro flutter_dipnav._DipNavRouter.initRoute}
  DipNavRoute get initRoute => _delegate.initRoute;

  /// {@macro flutter_dipnav._DipNavRouter.notFoundRoute}
  ///
  /// [routes] is a list of routes to parse from URL change in the browser
  /// no need to add initialization and not found routes to this list
  DipNavRoute? get notFoundRoute => _delegate.notFoundRoute;

  /// Push method of adding new route in stack
  Future<T?> push<T>(
      {String? key,
      required String path,
      Map<String, String>? queries,
      required Widget page,
      final Object? arguments,
      bool removeUntil = false,
      bool replaceCurrent = false,
      bool rebuild = true,
      bool fullscreenDialog = false,
      bool maintainState = true}) {
    return pushRoute<T>(
        route: DipNavRoute(
          key: key,
          path: path,
          page: page,
          queries: queries,
          arguments: arguments,
        ),
        removeUntil: removeUntil,
        replaceCurrent: replaceCurrent,
        rebuild: rebuild,
        fullscreenDialog: fullscreenDialog,
        maintainState: maintainState);
  }

  /// Push route method of adding new route in stack
  Future<T?> pushRoute<T>(
      {required DipNavRoute route,
      bool removeUntil = false,
      bool replaceCurrent = false,
      bool rebuild = true,
      bool fullscreenDialog = false,
      bool maintainState = true}) {
    return _delegate.pushRoute(route,
        removeUntil: removeUntil,
        replaceCurrent: replaceCurrent,
        rebuild: rebuild,
        fullscreenDialog: fullscreenDialog,
        maintainState: maintainState);
  }

  /// Pop method for return to previous route
  Future<bool> pop<T>({T? arguments, bool rebuild = true}) {
    return _delegate.popRoute();
  }

  bool get canPop => _delegate.canPop();
}
