part of 'dip_nav_router.dart';

/// A page that creates a material style [PageRoute].
///
/// {@macro flutter.material.materialRouteTransitionMixin}
class _DipNavResultablePage extends MaterialPage<DipNavRoute> {
  final DipNavRoute route;

  _DipNavResultablePage({
    required this.route,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) : super(
            child: route.page,
            maintainState: maintainState,
            fullscreenDialog: fullscreenDialog,
            key: ValueKey<String>(route.key ?? route.path.toString()),
            name: route.uri.toString(),
            arguments: route.data?.toMap());
}
