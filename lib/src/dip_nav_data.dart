class DipNavData {
  final Map<String, String>? queries;
  final Object? arguments;

  const DipNavData({required this.queries, required this.arguments});

  factory DipNavData.fromMap(Map<String, dynamic> map) {
    final Map<String, String>? queries = map['queries'];
    final Map<String, dynamic>? arguments = map['arguments'];
    return DipNavData(queries: queries, arguments: arguments);
  }

  Map<String, dynamic> toMap() {
    final map = <String, dynamic>{};
    map['queries'] = queries;
    map['arguments'] = arguments;
    return map;
  }
}
