part of 'dip_nav_router.dart';

/// A delegate that is used by the [Router] widget to build and configure a
/// navigating widget.
class DipNavDelegate extends RouterDelegate<DipNavRoute>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<DipNavRoute> {
  /// The list of [ResultablePage]
  final List<_DipNavResultablePage> _stackPages = [];

  /// The list of [DipNavRoute]
  final List<DipNavRoute> routes;

  /// The [DipNavRoute]
  final DipNavRoute initRoute;

  /// The [DipNavRoute]
  final DipNavRoute? notFoundRoute;

  /// The Navigator Key
  final GlobalKey<NavigatorState>? key;

  final List<NavigatorObserver>? dipNavObservers;
  final TransitionDelegate<dynamic>? transitionDelegate;

  DipNavDelegate(
      {this.key,
      required this.initRoute,
      this.notFoundRoute,
      this.routes = const [],
      this.dipNavObservers,
      this.transitionDelegate});

  @override
  Future<void> setInitialRoutePath(DipNavRoute configuration) {
    if (_stackPages.isEmpty) return setNewRoutePath(configuration);
    return Future.value();
  }

  /// Called by the [Router] at startup with the structure that the
  /// [RouteInformationParser] obtained from parsing the initial route.
  @override
  Widget build(BuildContext context) {
    if (_stackPages.isEmpty) {
      return Container(color: Colors.white);
    }
    return Navigator(
      key: navigatorKey,
      onPopPage: _onPopPage,
      pages: List.from(_stackPages),
      transitionDelegate:
          transitionDelegate ?? const DefaultTransitionDelegate<dynamic>(),
    );
  }

  @override
  GlobalKey<NavigatorState> get navigatorKey => GlobalKey<NavigatorState>();

  @override
  Future<void> setNewRoutePath(DipNavRoute configuration) {
    _stackPages.clear();
    return pushRoute(configuration);
  }

  /// Called by the [Router] when it detects a route information may have
  /// changed as a result of rebuild.
  @override
  DipNavRoute? get currentConfiguration {
    return _stackPages.isNotEmpty ? _stackPages.last.route : null;
  }

  Future<T?> pushRoute<T>(DipNavRoute route,
      {bool removeUntil = false,
      bool replaceCurrent = false,
      bool rebuild = true,
      bool fullscreenDialog = false,
      bool maintainState = true}) async {
    assert(!(removeUntil && replaceCurrent),
        'Only removeUntil or replaceCurrent should by true!');
    if (removeUntil) {
      _stackPages.clear();
    } else if (replaceCurrent && _stackPages.isNotEmpty) {
      _stackPages.removeLast();
    }
    _addRoute(route,
        fullscreenDialog: fullscreenDialog, maintainState: maintainState);
    if (rebuild) {
      notifyListeners();
      dipNavObservers?.forEach((element) {
        if (!replaceCurrent && !removeUntil) {
          if (kDebugMode) print('didPush');
          element.didPush(
              _stackPages.last.route,
              _stackPages.length >= 2
                  ? _stackPages[_stackPages.length - 2].route
                  : null);
        } else if (replaceCurrent) {
          if (kDebugMode) print('didReplace');
          element.didReplace(
              newRoute: _stackPages.last.route,
              oldRoute: _stackPages.length >= 2
                  ? _stackPages[_stackPages.length - 2].route
                  : null);
        } else {
          if (kDebugMode) print('didReplace');
          element.didRemove(
              _stackPages.last.route,
              _stackPages.length >= 2
                  ? _stackPages[_stackPages.length - 2].route
                  : null);
        }
      });
    }
    return Future.value();
  }

  @override
  Future<bool> popRoute() {
    if (_stackPages.length > 1) {
      dipNavObservers?.forEach((element) {
        element.didPop(
            _stackPages[_stackPages.length - 2].route, _stackPages.last.route);
      });
      _removePage(_stackPages.last);
      return Future.value(true);
    }
    return Future.value(false);
  }

  bool canPop() {
    return _stackPages.length > 1;
  }

  bool _onPopPage(Route<dynamic> route, dynamic result) {
    final didPop = route.didPop(result);
    if (!didPop) {
      return false;
    }
    _stackPages.remove(route.settings);
    notifyListeners();
    return true;
  }

  void _removePage(_DipNavResultablePage page) {
    _stackPages.remove(page);
    notifyListeners();
  }

  _DipNavResultablePage _createPage(DipNavRoute route,
      {bool fullscreenDialog = false, bool maintainState = true}) {
    return _DipNavResultablePage(
      route: route,
      fullscreenDialog: fullscreenDialog,
      maintainState: maintainState,
    );
  }

  void _addRoute(DipNavRoute route,
      {bool fullscreenDialog = false, bool maintainState = true}) {
    _stackPages.add(
      _createPage(route,
          fullscreenDialog: fullscreenDialog, maintainState: maintainState),
    );
  }
}
