library dipnav;

import 'package:flutter/widgets.dart';
import 'package:flutter_dipnav/src/dip_nav_data.dart';
import 'package:flutter_dipnav/src/dip_nav_route.dart';
import 'package:flutter_dipnav/src/dip_nav_router.dart';

export 'src/dip_nav_data.dart';
export 'src/dip_nav_route.dart';
export 'src/dip_nav_router.dart';

/// Extension to get [DipNavRouter] from the [BuildContext].
extension DipNavExtension on BuildContext {
  /// Get Router
  DipNavRouter get dipNav {
    return DipNav.of(this).router;
  }

  /// Get data from routing
  DipNavData? get dipNavData {
    final args = ModalRoute.of(this)?.settings.arguments;
    if (args != null && args is Map) {
      return DipNavData.fromMap(args as Map<String, dynamic>);
    }
    return null;
  }

  /// Get routing path
  Uri? get dipNavUri {
    final name = ModalRoute.of(this)?.settings.name;
    return name != null ? Uri.parse(name) : null;
  }
}

/// The widget that propagate [DipNavRouter] down the tree.
class DipNav extends InheritedWidget {
  /// {@macro flutter_dipnav.DipNavRouter}
  final DipNavRouter router;
  final Key? key;

  const DipNav({
    this.key,
    required this.router,
    required Widget child,
  }) : super(child: child);

  /// Builder of DipNav widget
  ///
  /// [initRoute] {@macro flutter_dipnav.DipNavRoute}
  /// [notFoundRoute] {@macro flutter_dipnav.DipNavRoute}
  /// [routes] List of {@macro flutter_dipnav.DipNavRoute}
  /// [transitionDelegate] {@macro flutte.TransitionDelegate}
  factory DipNav.builder({
    GlobalKey<NavigatorState>? key,
    required DipNavRoute initRoute,
    required Widget Function(DipNavDelegate delegate, DipNavParser parser,
            DipNavDispatcher dispatcher)
        builder,
    DipNavRoute? notFoundRoute,
    List<DipNavRoute>? routes,
    List<NavigatorObserver>? dipNavObservers,
    TransitionDelegate<dynamic>? transitionDelegate,
  }) {
    final dipNav = DipNavRouter(
      key: key,
      initRoute: initRoute,
      notFoundRoute: notFoundRoute,
      routes: routes,
      dipNavObservers: dipNavObservers,
      transitionDelegate: transitionDelegate,
    );
    return DipNav(
      key: key,
      router: dipNav,
      child: Builder(
        builder: (context) =>
            builder(dipNav.delegate, dipNav.parser, dipNav.dispatcher),
      ),
    );
  }

  static DipNav of(BuildContext context) {
    final DipNav? result = context.dependOnInheritedWidgetOfExactType<DipNav>();
    assert(result != null, 'No DipNav found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(covariant DipNav oldWidget) {
    return router != oldWidget.router;
  }
}
