# Versions

## 1.0.0-dev.10 - ***[June 17, 2021]***

Fix observer events

## 1.0.0-dev.9 - ***[May 12, 2021]***

Fix canPop

## 1.0.0-dev.8 - ***[May 8, 2021]***

Change generation init route

## 1.0.0-dev.7 - ***[April 8, 2021]***

Delete navigation observer

## 1.0.0-dev.6 - ***[Marth 26, 2021]***

Add navigation observer and transition delegate to widget

## 1.0.0-dev.5 - ***[Marth 26, 2021]***

Change image

## 1.0.0-dev.4 - ***[Marth 26, 2021]***

+ Change DipNav widget - add builder
+ Add guard for route
+ Add route list

## 1.0.0-dev.3 - ***[Marth 22, 2021]***

Adding arguments

## 1.0.0-dev.2 - ***[Marth 21, 2021]***

Change description

## 1.0.0-dev.1 - ***[Marth 21, 2021]***

Initial release